---
layout: post
title:  "GitLab CI - Stages: Test"
date:   2016-02-26 14:08:10 -0200
categories: test
---

# GitLab CI - Stages

## Testing config:

{% highlight yaml %}
image: ruby:2.1

cache:
  paths: 
    - vendor/

before_script:
  - bundle install --path vendor

test:
  stage: test
  script:
    - bundle exec jekyll build -d public/
  except:
    - master

pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public/
  artifacts:
    paths:
      - public
  only:
    - master
{% endhighlight %}